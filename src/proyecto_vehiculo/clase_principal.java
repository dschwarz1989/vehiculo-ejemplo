package proyecto_vehiculo;

import java.util.ArrayList;

public class clase_principal {

	public static void main(String[] args) {

		ArrayList<Vehiculo> ArrayVeh = new ArrayList<Vehiculo>();

		Vehiculo vh1 = new Vehiculo();

		vh1.setMarca("Ford");

		vh1.setColor("Azul");

		vh1.setModelo(1990);

		vh1.setPatente("UHT240");

		ArrayVeh.add(vh1);

		Vehiculo vh2 = new Vehiculo();

		vh2.setMarca("Chevy");

		vh2.setColor("Rojo");

		vh2.setModelo(1999);

		vh2.setPatente("AB00845");

		ArrayVeh.add(vh2);

		Vehiculo vh3 = new Vehiculo();

		vh3.setMarca("Peugeot");

		vh3.setColor("Negro");

		vh3.setModelo(2018);

		vh3.setPatente("AB004YR");

		ArrayVeh.add(vh3);
		
		
		
		// invocacion
		calculeAutoAntiguo(ArrayVeh);
		
		calculeAutoUltomoModelo(ArrayVeh);

	} // FIN DEL MAIN

	

	//dfinicion de lo que hace la funcion
	private static void calculeAutoAntiguo(ArrayList<Vehiculo> arrayVeh) {

		Vehiculo vAntiguo = new Vehiculo();

		int vuelta = 0;

		
		
		
		for (Vehiculo vehiculoTemporal : arrayVeh) {

			vuelta = vuelta + 1;

			if (vuelta == 1) {

				vAntiguo = vehiculoTemporal;
				
			} else if (vAntiguo.getModelo() > vehiculoTemporal.getModelo()) {

				vAntiguo = vehiculoTemporal;

			}

		}
		
		System.out.println("El auto mas antiguo es " + vAntiguo.getPatente() + ", el modelo es " + vAntiguo.getModelo() );

	}
	
	private static void calculeAutoUltomoModelo(ArrayList<Vehiculo> arrayVeh) {
		Vehiculo ultimoModelo = new Vehiculo();

		int vuelta = 0;

		for (Vehiculo vehiculoTemporal : arrayVeh) {

			vuelta = vuelta + 1;

			if (vuelta == 1) {

				ultimoModelo = vehiculoTemporal;
				
			} else if (ultimoModelo.getModelo() < vehiculoTemporal.getModelo()) {

				ultimoModelo = vehiculoTemporal;

			}

		}
		
		
		System.out.println("El auto �ltimo modelo  es " + ultimoModelo.getPatente() + ", el modelo es " + ultimoModelo.getModelo() );

	}

}
