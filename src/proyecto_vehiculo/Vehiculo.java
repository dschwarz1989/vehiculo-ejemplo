package proyecto_vehiculo;

public class Vehiculo {

	// atributos
	private String marca;

	private Integer modelo;

	private String patente;

	private String color;

	// constructor vacio
	public Vehiculo() {

	}

	
	// Constructor cargado

	public Vehiculo(String col, Integer mod) {
		this.color = col;
		this.modelo = mod;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public Integer getModelo() {
		return modelo;
	}

	public void setModelo(Integer modelo) {
		this.modelo = modelo;
	}

	public String getPatente() {
		return patente;
	}

	public void setPatente(String patente) {
		this.patente = patente;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	
	
}
